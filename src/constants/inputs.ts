// import { v4 as uuidv4 } from "uuid";

// // Attributes
// import { authAttributes } from "./authAttributes";

// // Placeholders
// import { authPlaceholders } from "./authPlaceholders";
// import CustomSelect from "@/components/CustomSelect";

// export interface Field {
//     id: string;
//     type: string;
//     name: string;
//     placeholder: string;
//     component?: JSX.Element;
// };

// export const registerFields: Field[] =  [
//     {
//         id: uuidv4(),
//         type: "text",
//         name: authAttributes.USERNAME_ATTRIBUTE,
//         placeholder: authPlaceholders.NAME_PLACEHOLDER,
//     },
//     {
//         id: uuidv4(),
//         type: "text",
//         name: authAttributes.EMAIL_ATTRIBUTE,
//         placeholder: authPlaceholders.EMAIL_PLACEHOLDER,
//     },
//     {
//         id: uuidv4(),
//         type: "text",
//         name: authAttributes.PHONE_NUMBER_ATTRIBUTE,
//         placeholder: authPlaceholders.PHONE_NUMBER_PLACEHOLDER,
//     },
//     {
//         id: uuidv4(),
//         type: "text",
//         name: authAttributes.LEVEL_ATTRIBUTE,
//         placeholder: authPlaceholders.LEVEL_PLACEHOLDER,
//         component: CustomSelect
//     },
//     {
//         id: uuidv4(),
//         type: "text",
//         name: authAttributes.SUBJECT_ATTRIBUTE,
//         placeholder: authPlaceholders.SUBJECT_PLACEHOLDER,
//         component: CustomSelect
//     },
//     {
//         id: uuidv4(),
//         type: "password",
//         name: authAttributes.PASSWORD_ATTRIBUTE,
//         placeholder: authPlaceholders.PASSWORD_PLACEHOLDER,
//     },
//     {
//         id: uuidv4(),
//         type: "password",
//         name: authAttributes.CONFIRM_PASSWORD_ATTRIBUTE,
//         placeholder: authPlaceholders.PASSWORD_PLACEHOLDER,
//     },
// ];
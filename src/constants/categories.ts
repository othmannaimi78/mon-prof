import { v4 as uuidv4 } from 'uuid';

export interface Category {
	id: string;
	label: string;
	value: string;
	slug?: string;
	price?: number;
}

export interface SubjectData {
	uuid: string;
	label: string;
	value: string;
	slug: string;
	price: number;
}

export const studyLevels: Category[] = [
	{
		id: uuidv4(),
		label: 'Sciences Mathématiques A et B',
		value: 'Sciences Mathématiques A et B',
	},
	{
		id: uuidv4(),
		label: 'Sciences Physiques',
		value: 'Sciences Physiques',
	},
	{
		id: uuidv4(),
		label: 'Sciences de la Vie et de la Terre (SVT)',
		value: 'Sciences de la Vie et de la Terre (SVT)',
	},
];

export const subjects: Category[] = [
	{
		id: uuidv4(),
		label: 'Mathématiques (Arabe)',
		value: 'Mathématiques (Arabe)',
		slug: 'math',
		price: 1,
	},
	{
		id: uuidv4(),
		label: 'Mathématiques (Français)',
		value: 'Mathématiques (Français)',
		slug: 'math',
		price: 1,
	},
	{
		id: uuidv4(),
		label: 'Physiques et Chimie (Arabe)',
		value: 'Physiques et Chimie (Arabe)',
		slug: 'physics-chemistry',
		price: 1,
	},
	{
		id: uuidv4(),
		label: 'Physiques et Chimie (Français)',
		value: 'Physiques et Chimie (Français)',
		slug: 'physics-chemistry',
		price: 1,
	},
	{
		id: uuidv4(),
		label: 'Sciences de la Vie et de la Terre (SVT Arabe)',
		value: 'Sciences de la Vie et de la Terre (SVT Arabe)',
		slug: 'svt',
		price: 1,
	},
	{
		id: uuidv4(),
		label: 'Sciences de la Vie et de la Terre (SVT Français)',
		value: 'Sciences de la Vie et de la Terre (SVT Français)',
		slug: 'svt',
		price: 1,
	},
];

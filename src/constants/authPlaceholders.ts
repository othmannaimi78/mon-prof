import { KeyValue } from "./commonTypes";

export const authPlaceholders: KeyValue = {
    NAME_PLACEHOLDER: "الإسم و النسب",
    EMAIL_PLACEHOLDER: "البريد الإلكتروني",
    PHONE_NUMBER_PLACEHOLDER: "مثال: 0622661465*",
    LEVEL_PLACEHOLDER: "المستوى : التاني باكالوريا",
    SUBJECT_PLACEHOLDER: "المواد",
    PASSWORD_PLACEHOLDER: "***********",
};
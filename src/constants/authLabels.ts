import { KeyValue } from "./commonTypes";

export const authLabels: KeyValue = {
    NAME_LABEL: "الإسم و النسب",
    EMAIL_LABEL: "البريد الإلكتروني",
    PHONE_NUMBER_LABEL: "رقم الهاتف",
    LEVEL_LABEL: "المستوى : التاني باكالوريا",
    SUBJECT_LABEL: "المواد",
    PASSWORD_LABEL: "كلمة المرور",
    CONFIRM_PASSWORD_LABEL: "إعادة كلمة المرور"
};
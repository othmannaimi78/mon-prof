import { v4 as uuidv4 } from 'uuid';

export interface Class {
	uuid: string;
	startTime: string;
	endTime: string;
	day: string;
}

export interface Teacher {
	uuid: string;
	firstName: string;
	lastName: string;
	avatar: string;
}

export interface Subject {
	uuid: string;
	name: string;
	slug: string;
	teachers: Teacher[];
}

export const subjects: Subject[] = [
	{
		uuid: uuidv4(),
		name: 'Mathématiques (Arabe)',
		slug: 'math',
		teachers: [],
	},
	{
		uuid: uuidv4(),
		name: 'Mathématiques (Français)',
		slug: 'math',
		teachers: [],
	},
	{
		uuid: uuidv4(),
		name: 'Physiques et Chimie (Arabe)',
		slug: 'physics-chemistry',
		teachers: [],
	},
	{
		uuid: uuidv4(),
		name: 'Physiques et Chimie (Français)',
		slug: 'physics-chemistry',
		teachers: [],
	},
	{
		uuid: uuidv4(),
		name: 'Sciences de la Vie et de la Terre (SVT Arabe)',
		slug: 'svt',
		teachers: [
			{
				uuid: uuidv4(),
				firstName: 'mohamed',
				lastName: 'ahgoug',
				avatar: '/images/mohamed-ahgoug.jpeg',
			},
		],
	},
	{
		uuid: uuidv4(),
		name: 'Sciences de la Vie et de la Terre (SVT Français)',
		slug: 'svt',
		teachers: [
			{
				uuid: uuidv4(),
				firstName: 'mohamed',
				lastName: 'ahgoug',
				avatar: '/images/mohamed-ahgoug.jpeg',
			},
		],
	},
];

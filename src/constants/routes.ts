import { v4 as uuidv4 } from 'uuid';

export interface Route {
	id: string;
	href: string;
	title: string;
	icon?: string;
}

export const mainRoutes: Route[] = [
	{
		id: uuidv4(),
		href: '/contact-us',
		title: 'تواصل معنا',
	},
	{
		id: uuidv4(),
		href: '/services',
		title: 'خدماتنا',
	},
	{
		id: uuidv4(),
		href: '/about',
		title: 'من نحن',
	},
	{
		id: uuidv4(),
		href: '/',
		title: 'الرئيسية',
	},
];

export const dashboardRoutes: Route[] = [
	{
		id: uuidv4(),
		href: '/dashboard',
		title: 'المعلومات شخصية',
		icon: '/icons/personal-details-icon.svg',
	},
	{
		id: uuidv4(),
		href: '/dashboard/subjects',
		title: 'المواد',
		icon: '/icons/subjects-icon.svg',
	},
];

export const ZOOM_OAUTH_ENDPOINT = 'https://zoom.us/oauth/token';
export const ZOOM_API_BASE_URL = 'https://api.zoom.us/v2';
export const LIVE_CHECK_HEALTH = '/api/hello';
export const ZOOM_MEETING_API = '/api/zoom';

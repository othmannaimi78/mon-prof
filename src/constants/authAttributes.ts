// common
import { KeyValue } from "./commonTypes";

export const authAttributes: KeyValue = {
  NAME_ATTRIBUTE: "name",
  EMAIL_ATTRIBUTE: "email",
  PHONE_NUMBER_ATTRIBUTE: "phoneNumber",
  LEVEL_ATTRIBUTE: "level",
  SUBJECT_ATTRIBUTE: "subjects",
  PASSWORD_ATTRIBUTE: "password",
  CONFIRM_PASSWORD_ATTRIBUTE: "confirmPassword",
};

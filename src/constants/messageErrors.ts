interface MessageError {
    [key: string]: string;
 }

export const messageErrors: MessageError = {
    FIELD_REQUIRED_MESSAGE_ERROR: "الحقل مطلوب",
    EMAIL_NOT_VALID_MESSAGE_ERROR: "البريد الإلكتروني غير صالح",
    PASSWORD_MAX_LENGTH_MESSAGE_ERROR: "يجب أن تكون كلمة المرور أكبر من 6 أحرف",
    PASSWORD_NOT_MATCH_MESSAGE_ERROR: "كلمة المرور غير متطابقة"
};
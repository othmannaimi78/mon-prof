'use client';

// Components
import { Header } from '@/components/Header';

import { auth } from '@/firebaseConfig';
import { useRouter } from 'next/navigation';
import { useAuthState } from 'react-firebase-hooks/auth';
import { ClipLoader } from 'react-spinners';

export const MainLayout: React.FC<{ children: JSX.Element }> = ({
	children,
}) => {
	const [user, loading] = useAuthState(auth);

	const router = useRouter();

	if (loading) {
		return (
			<div className='flex justify-center items-center w-full h-screen'>
				<ClipLoader
					color='green'
					loading={loading}
					size={50}
					aria-label='Loading Spinner'
					data-testid='loader'
				/>
			</div>
		);
	}

	if (user) {
		router.push('/dashboard');
		return null;
	}

	return (
		<main>
			<Header />
			{children}
		</main>
	);
};

// TODO: validate file type
// TODO: add unit test
export const isValidType = (type?: string): boolean => {
	console.log('isValidType: ', type);
	const allowedExtensions = [
		'image/jpg',
		'image/jpeg',
		'image/png',
		'application/pdf',
	];

	if (!type) return false;

	if (allowedExtensions.includes(type)) return true;

	return false;
};

// TODO: add unit test
export const isValidSize = (size?: number): boolean => {
	const ALLOW_MAX_FILE_SIZE = 5_000_000;

	if (!size) return false;

	if (size <= ALLOW_MAX_FILE_SIZE) return true;

	return false;
};

export const generateUniqueId = (): string =>
	Math.random().toString(36).substring(2, 10);

import { FirebaseError, initializeApp } from 'firebase/app';
import {
	GoogleAuthProvider,
	getAuth,
	signInWithPopup,
	signInWithEmailAndPassword,
	createUserWithEmailAndPassword,
	sendPasswordResetEmail,
	signOut,
} from 'firebase/auth';
import {
	getFirestore,
	query,
	getDocs,
	collection,
	where,
	addDoc,
} from 'firebase/firestore';
import { getStorage } from 'firebase/storage';
import { RegisterValues, loginValues } from './schema/auth';

const firebaseConfig = {
	apiKey: process.env.NEXT_PUBLIC_FIREBASE_API_KEY,
	authDomain: process.env.NEXT_PUBLIC_FIREBASE_AUTH_DOMAIN,
	projectId: process.env.NEXT_PUBLIC_FIREBASE_PROJECT_ID,
	storageBucket: process.env.NEXT_PUBLIC_FIREBASE_STORAGE_BUCKET,
	messagingSenderId: process.env.NEXT_PUBLIC_FIREBASE_MESSAGING_SENDER_ID,
	appId: process.env.NEXT_PUBLIC_FIREBASE_APP_ID,
	measurementId: process.env.NEXT_PUBLIC_MEASUREMENT_ID,
};

// Initialize Firebase
// const analytics = getAnalytics(app);

const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const db = getFirestore(app);
// Initialize Cloud Storage and get a reference to the service
const storage = getStorage(app);

const googleProvider = new GoogleAuthProvider();

// const signInWithGoogle = async () => {
//   try {
//     const res = await signInWithPopup(auth, googleProvider);
//     const user = res.user;
//     const q = query(collection(db, "users"), where("uid", "==", user.uid));
//     const docs = await getDocs(q);
//     if (docs.docs.length === 0) {
//       await addDoc(collection(db, "users"), {
//         uid: user.uid,
//         name: user.displayName,
//         authProvider: "google",
//         email: user.email,
//       });
//     }
//   } catch (err) {
//     console.error(err);
//     alert(err.message);
//   }
// };

const logInWithEmailAndPassword = async (userInfo: loginValues) => {
	try {
		const { email, password } = userInfo;
		await signInWithEmailAndPassword(auth, email, password);
	} catch (error: unknown) {
		if (error instanceof FirebaseError) {
			console.error('firebase login user got an exception: ', error.code);
			if (error.code === 'auth/user-not-found') {
				throw Error('auth/user-not-found');
			}
		}
	}
};

const registerWithEmailAndPassword = async (userInfo: RegisterValues) => {
	try {
		const {
			email,
			name,
			password,
			phoneNumber,
			subjects,
			level,
			isSubscribed,
			subscriptionDate,
			creationDate,
			docUrl,
		} = userInfo;
		const res = await createUserWithEmailAndPassword(auth, email, password);
		const user = res.user;
		await addDoc(collection(db, 'users'), {
			uid: user.uid,
			authProvider: 'local',
			email,
			phoneNumber,
			name,
			subjects,
			level,
			isSubscribed,
			subscriptionDate,
			creationDate,
			docUrl,
		});
	} catch (error: unknown) {
		if (error instanceof FirebaseError) {
			console.error('firebase register user got an exception: ', error.code);
			if (error.code === 'auth/email-already-in-use') {
				throw Error('auth/email-already-in-use');
			}
		}
	}
};

// const sendPasswordReset = async (email) => {
//   try {
//     await sendPasswordResetEmail(auth, email);
//     alert("Password reset link sent!");
//   } catch (err) {
//     console.error(err);
//     alert(err.message);
//   }
// };

const logout = () => {
	signOut(auth);
};

export {
	auth,
	db,
	storage,
	// signInWithGoogle,
	logInWithEmailAndPassword,
	registerWithEmailAndPassword,
	// sendPasswordReset,
	logout,
};

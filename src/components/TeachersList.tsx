// Constants
import { Teacher } from "@/constants/subjects";

// Components
import { TeacherInfo } from "./TeacherInfor";

export const TeachersList: React.FC<{ teachersList: Teacher[] | undefined; }> = ({ teachersList }) => {
    return (
        <div className="grid grid-cols-2 md:grid-cols-4">
            {
                teachersList?.map((teacher: Teacher) => (
                    <TeacherInfo
                        key={teacher.uuid}
                        imageUrl={teacher.avatar}
                        firstName={teacher.firstName}
                        lastName={teacher.lastName}
                    />
                ))
            }
        </div>
    );
};
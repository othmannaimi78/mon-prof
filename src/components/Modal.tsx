import React, { ReactNode, useState } from 'react';

export const Modal: React.FC<{ 
  children: ReactNode, 
  isOpen: boolean, 
}> 
  = ({ children, isOpen }) => {

  return (
    <>
      {isOpen && (
        <div className="fixed inset-0 flex items-center justify-center z-10">
          <div className="absolute inset-0 bg-black opacity-50" />
          <div className="bg-white p-8 rounded-lg z-20 w-full md-w-1/2">
            {children}
          </div>
        </div>
      )}
    </>
  );
};

export default Modal;

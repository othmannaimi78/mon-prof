// Components
import { Countdown } from './Countdown';

export const NextClasses: React.FC = () => {
	// Specify the launch time
	const launchTime = new Date('May 22, 2023 16:48:10').getTime();

	return (
		<div className='flex items-center justify-between pb-8'>
			<Countdown launchTime={launchTime} />
			<span>سيبدأ الفصل المباشر التالي في الساعة 14</span>
			<span>يوسف البقال</span>
		</div>
	);
};

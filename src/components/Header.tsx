// Components
import { MobileNavbar } from "./MobileNavbar";
import { Navbar } from "./Navbar";

export const Header: React.FC = () => {
    return (
        <header className="container mx-auto">
            <Navbar />
            <MobileNavbar />
        </header>
    );
};

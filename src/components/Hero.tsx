import Link from "next/link";
import Image from "next/image";

export const Hero: React.FC = () => {
    return (
        <section className="container mx-auto flex flex-col mb-24 md:mt-12 md:flex-row">
            <div 
                className="mb-8 w-full md:w-1/2 h-screen md:mb-0" 
                style={{ 
                    backgroundImage: `url(https://www.gettingsmart.com/wp-content/uploads/2017/01/Girl-with-Headphones-Using-Laptop-Listening-to-Podcast-Feature-Image.jpg)`, backgroundRepeat: 'no-repeat', 
                    backgroundSize: 'cover', 
                    backgroundPosition: 'center', 
                    boxShadow: 'rgb(244, 244, 246) 72px 72px 0px 0px', 
                    borderRadius: 20 
                }}
            >

            </div>
            <div className="text-right mb-8 md:mb-0 md:p-20">
                <div className="mb-12">
                    <h1 className="text-5xl font-bold mb-4">بجانبك اينما كنت من اجل نجاحك</h1>
                    <p className="text-2xl">احصل على دروس الدعم عن بعد في جميع المواد</p>
                    <p className="text-2xl">.على شكل حصص مباشرة</p>
                </div>
                <Link href="/register">
                    <span className="bg-darkGreen px-4 py-2 cursor-pointer text-white rounded-lg text-2xl font-bold shadow-lg hover:bg-mainGreen">
                        ! استفد من أسبوع مجانا
                    </span>
                </Link>
                <div className="mt-20">
                    <p className="text-right flex justify-end text-2xl">
                        استعد لمتحاناتك من بيتك
                        <Image
                            width={24}
                            height={24}
                            alt="check-circle-icon"
                            src="/icons/check-circle-svgrepo-com.svg"
                            className="ml-4"
                        />
                    </p>
                    <p className="text-right flex justify-end text-2xl">
                    اختر التوقيت المناسب لك لحضور الحصة
                        <Image
                            width={24}
                            height={24}
                            alt="check-circle-icon"
                            src="/icons/check-circle-svgrepo-com.svg"
                            className="ml-4"
                        />
                    </p>
                    <p className="text-right flex justify-end text-2xl">
                    استفد من افضل الاساتذة في المغرب
                        <Image
                            width={24}
                            height={24}
                            alt="check-circle-icon"
                            src="/icons/check-circle-svgrepo-com.svg"
                            className="ml-4"
                        />
                    </p>
                    <p className="text-right flex justify-end text-2xl">
                        تحصل على جميع الدروس فور انتهاء الحصة
                        <Image
                            width={24}
                            height={24}
                            alt="check-circle-icon"
                            src="/icons/check-circle-svgrepo-com.svg"
                            className="ml-4"
                        />
                    </p>
                </div>
            </div>
        </section>
    );
};
export const TeacherInfo: React.FC<{ firstName: string; lastName: string; imageUrl: string; }> = ({
    firstName,
    lastName,
    imageUrl
}) => {
    return (
        <div className="flex flex-col items-center">
            <img
                src={imageUrl}
                alt={firstName}
                className="rounded-full h-32 w-32 mb-8"
            />
            <h2 className="text-xl text-center uppercase">{firstName} {lastName}</h2>
        </div>
    );
};
import { weekdays } from '@/constants/weekdays';
import { CalendarData } from '@/interfaces/calendarData';
import React from 'react';

interface CalendarProps {
  data?: CalendarData[];
  timeSlots: string[];
}

export const Calendar: React.FC<CalendarProps> = ({ data, timeSlots }) => {

  return (
    <div className="container mx-auto pb-14">
      <div className="overflow-x-auto">
        <table className="min-w-full">
          <thead>
            <tr>
              <th className="border px-4 py-2"></th>
              {timeSlots.map((slot, index) => (
                <th className="border px-4 py-2" key={index}>
                  {slot}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {weekdays.map((day: string) => (
              <tr key={day}>
                <td className="border px-4 py-2">{day}</td>
                {timeSlots.map((slot, index) => {
                  const filteredData = data?.filter(
                    item => item.dayName === day && (item.startTime === slot || item.endTime === slot)
                  );
                  return (
                    <td className="border px-4 py-2" key={index}>
                      {filteredData?.map(item => (
                        <div className='uppercase' key={`${item.firstName}-${item.lastName}`}>
                          {item.firstName} {item.lastName}
                        </div>
                      ))}
                    </td>
                  );
                })}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

// Libs
import React, { ComponentType, useState } from "react";
import { Field, ErrorMessage } from "formik";

// Interfaces
import Image from "next/image";

// Components
import TextError from "./TextError";

export type InputProps = {
    type?: string;
    title?: string;
    placeholder?: string;
    label?: string;
    name: string;
    className?: string;
    passwordInputContainer?: string;
    passwordInput?: string;
  };

const Input: React.FC<InputProps> = ({
  type = "text",
  name,
  placeholder,
  className,
  passwordInputContainer = "",
  passwordInput = "",
  ...rest
}) => {
  // States
  const [showPassword, setShowPassword] = useState<boolean>(false);

  const toggleShowPassword = () => setShowPassword((oldState) => !oldState);

  if (type === "password") {
    return (
      <>
        <ErrorMessage name={name} component={TextError as ComponentType} />
        <div className={passwordInputContainer}>
          <Field
            type={!showPassword ? type : "text"}
            name={name}
            placeholder={placeholder}
            className={passwordInput}
            autoComplete="off"
            {...rest}
          />
          {showPassword ? (
            <Image 
                width={16}
                height={16}
                alt="eye-close-icon"
                src="/icons/eye-close.svg"
                onClick={toggleShowPassword}
            />
          ) : (
            <Image 
                width={16}
                height={16}
                alt="eye-close-icon"
                src="/icons/eye-open.svg"
                onClick={toggleShowPassword}
            />
          )}
        </div>
      </>
    );
  }

  return (
    <>
      <ErrorMessage name={name} component={TextError as ComponentType} />
      <Field
        type={type}
        name={name}
        placeholder={placeholder}
        className={className}
        autoComplete="off"
        {...rest}
      />
    </>
  );
};

export default Input;
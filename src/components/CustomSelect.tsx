import { SubjectData } from '@/constants/categories';
import { FieldProps } from 'formik';
import React from 'react';
import Select from 'react-select';
import {
	Options,
	OnChangeValue,
} from 'react-select/dist/declarations/src/types';

interface Option {
	label: string;
	value: string;
}

interface CustomSelectProps extends FieldProps {
	// options: Options<Option>;
	options: SubjectData[];
	isMulti?: boolean;
	className?: string;
	placeholder?: string;
	defaultValues?: SubjectData
}

export const CustomSelect: React.FC<CustomSelectProps> = ({
	className,
	placeholder,
	field,
	form,
	options,
	isMulti = false,
	defaultValues
}: CustomSelectProps) => {
	const onChange = (option: OnChangeValue<Option, boolean>) => {
		form.setFieldValue(
			field.name,
			isMulti
				? (option as Option[]).map((item: Option) => item)
				: (option as Option).value
		);
	};

	const getValue = () => {
		if (options) {
			return isMulti
				? options.filter((option) => field.value.map((subject: Option) => subject.value).indexOf(option.value) >= 0)
				: options.find((option) => option.value === field.value);
		} else {
			return isMulti ? [] : ('' as any);
		}
	};

	return (
		<Select
			className={className}
			name={field.name}
			// value={getValue()}
			onChange={onChange}
			placeholder={placeholder}
			options={options}
			isMulti={isMulti}
			defaultValue={defaultValues}
		/>
	);
};

export default CustomSelect;

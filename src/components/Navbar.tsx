import { usePathname } from "next/navigation";
import Link from "next/link";

// Routes
import { Route, mainRoutes } from "@/constants/routes";

// Components
import { Logo } from "./Logo";

export const Navbar: React.FC = () => {

    // Hooks 
    const pathname = usePathname();

    return (
        <nav className="h-24 justify-between items-center hidden md:flex">
            <div className="flex justify-between items-center w-fit">
                <Link href="/login">
                    <span 
                    className={`rounded-full text-darkGreen border-mainGreen border-2 px-4 py-2 text-xl mr-4 cursor-pointer hover:bg-mainGreen hover:text-white`}
                    >
                        دخول
                    </span>
                </Link>
                <Link href="/register">
                    <span className="cursor-pointer rounded-full bg-darkGreen text-white px-4 py-2 text-xl hover:bg-mainGreen">
                        سجل مجانا
                    </span>
                </Link>
            </div>
            <div className="flex justify-center items-center flex-1">
                <div className="flex justify-between items-center w-fit space-x-10">
                    {
                        mainRoutes.map((routerLink: Route) => (
                            <Link key={routerLink.id} href={routerLink.href}>
                                <span 
                                    className={`transition ease-in-out duration-300 text-black text-xl cursor-pointer hover:text-darkGreen hover:font-bold ${pathname === routerLink.href ? "text-darkGreen": "text-black "}`}
                                >{routerLink.title}</span>
                            </Link>
                        ))
                    }
                </div>
            </div>
            <Logo href="/" />
        </nav>
    );
};
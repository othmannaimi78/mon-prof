import Link from 'next/link';

export const SubjectCard: React.FC<{
	subjectTitle: string;
	subjectLink: string;
}> = ({ subjectTitle, subjectLink }) => {
	return (
		<Link href={`/dashboard/teachers/${subjectLink}`}>
			<span className='bg-white shadow-rectangleShadow p-4 text-center cursor-pointer text-black block mb-4 mx-4'>
				{subjectTitle}
			</span>
		</Link>
	);
};

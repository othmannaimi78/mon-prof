'use client'
import { ErrorMessage, Field, Form, Formik, FormikProps } from "formik";
import { ClipLoader } from "react-spinners";
import { ComponentType, useState } from "react";
import { useRouter } from "next/navigation";

// Firebase config
import { registerWithEmailAndPassword } from "../firebaseConfig";

// Schema
import { RegisterValues } from "@/schema/auth";
import { registerSchemaValidation } from "@/schema/authValidation";

// Components
import CustomSelect from "./CustomSelect";
import TextError from "./TextError";

// Constants
import { studyLevels, subjects } from "@/constants/categories";
import { authAttributes } from "@/constants/authAttributes";
import { authPlaceholders } from "@/constants/authPlaceholders";
import { authLabels } from "@/constants/authLabels";
import { generateUniqueId } from "@/utils/generateUniqueId";
import { formatDateToCustomString } from "@/utils/formatDateToCustomString";

export const RegisterForm: React.FC = () => {

    // States
    const [loading, setLoading] = useState<boolean>(false);
    const [firebaseErrorMessage, setFirebaseErrorMessage] = useState<string | null>(null);

    // Hooks
    const router = useRouter();

    const initialValues: RegisterValues = {
        email: "",
        password: "",
        name: "",
        phoneNumber: "",
        level: "",
        subjects: [],
        confirmPassword: "",
        creationDate: formatDateToCustomString(new Date),
        isSubscribed: false,
        subscriptionDate: null,
        docUrl: null
    };

    const setUniqueIdInLocalStorage = (): void => {
        
        const DEVICE_ID_KEY = generateUniqueId();
        let deviceId = localStorage.getItem(DEVICE_ID_KEY);

        if (!deviceId) {
            deviceId = generateUniqueId();
            localStorage.setItem('DEVICE_ID', deviceId);
        };
    };

    const isAlreadyRegistered = (): boolean => {
        const deviceId = localStorage.getItem('DEVICE_ID');
        return deviceId != null;
    };

    const register = async (values: RegisterValues) => {
        if(isAlreadyRegistered()) {
            alert('لقد سبق لك التسجيل في حساب أخر');
            return router.push("/login");
        };

        setLoading(true);
        try {
            await registerWithEmailAndPassword(values)
            setUniqueIdInLocalStorage();
            setLoading(false);
            setFirebaseErrorMessage(null);
            router.push("/dashboard");
        } catch (error: unknown) {
            if (error instanceof Error) {
                if (error.message === "auth/email-already-in-use") {
                    setFirebaseErrorMessage("البريد الالكتروني موجود مسبقا");
                };
            } else {
                setFirebaseErrorMessage("من فضلك حاول لاحقا");
            };
            setLoading(false);
        };
    };

    return (
        <div className="xl:w-1/2 mt-10 mx-auto">
            <h1 className="text-center font-bold text-5xl mb-8">!تسجل الأن واستفد من أسبوع مجانا</h1>
            <Formik
                initialValues={initialValues}
                validationSchema={registerSchemaValidation}
                onSubmit={register}
            >
                {(props: FormikProps<any>) => (
                    <Form className="flex flex-col shadow-formShadow rounded-lg px-8 pb-16 h-fit text-gray1 sm:w-11/12 md:w-1/2 md:mx-auto xl:w-full">
                        <p className="text-right text-red-500 my-8">{firebaseErrorMessage && firebaseErrorMessage}</p>
                        <div className="flex flex-col">
                            <label htmlFor={authAttributes.NAME_ATTRIBUTE} className="text-right mb-4 font-bold">
                                <span className="text-red-500">*</span>
                                {authLabels.NAME_LABEL} 
                            </label>
                            <Field
                                type="text"
                                className="text-right border-2 p-2 text-black mb-4"
                                name={authAttributes.NAME_ATTRIBUTE}
                                placeholder={authPlaceholders.NAME_PLACEHOLDER}
                            />
                            <ErrorMessage
                                name={authAttributes.NAME_ATTRIBUTE}
                                component={TextError as ComponentType}
                            />
                        </div>

                        <div className="flex flex-col">
                            <label htmlFor={authAttributes.EMAIL_ATTRIBUTE} className="text-right mb-4 font-bold">
                                <span className="text-red-500">*</span>
                                {authLabels.EMAIL_LABEL}
                            </label>
                            <Field
                                type="text"
                                className="text-right border-2 p-2 text-black mb-4"
                                name={authAttributes.EMAIL_ATTRIBUTE}
                                placeholder={authPlaceholders.EMAIL_PLACEHOLDER}
                            />
                            <ErrorMessage
                                name={authAttributes.EMAIL_ATTRIBUTE}
                                component={TextError as ComponentType}
                            />
                        </div>


                        <div className="flex flex-col">
                            <label htmlFor={authAttributes.PHONE_NUMBER_ATTRIBUTE} className="text-right mb-4 font-bold">
                                <span className="text-red-500">*</span>
                                {authLabels.PHONE_NUMBER_LABEL}
                            </label>
                            <Field
                                type="text"
                                className="text-right border-2 p-2 text-black mb-4"
                                name={authAttributes.PHONE_NUMBER_ATTRIBUTE}
                                placeholder={authPlaceholders.PHONE_NUMBER_PLACEHOLDER}
                            />
                            <ErrorMessage
                                name={authAttributes.PHONE_NUMBER_ATTRIBUTE}
                                component={TextError as ComponentType}
                            />
                        </div>

                        <div className="flex flex-col">
                            <label htmlFor={authAttributes.LEVEL_ATTRIBUTE} className="text-right mb-4 font-bold">
                                <span className="text-red-500">*</span>
                                {authLabels.LEVEL_LABEL}
                            </label>
                            <Field
                                className="text-right mb-4"
                                name={authAttributes.LEVEL_ATTRIBUTE}
                                options={studyLevels}
                                component={CustomSelect}
                                placeholder={authPlaceholders.LEVEL_PLACEHOLDER}
                                isMulti={false}
                            />
                            <ErrorMessage
                                name={authAttributes.LEVEL_ATTRIBUTE}
                                component={TextError as ComponentType} />
                        </div>

                        <div className="flex flex-col">
                            <label htmlFor={authAttributes.SUBJECT_ATTRIBUTE} className="text-right mb-4 font-bold">
                                <span className="text-red-500">*</span>
                                {authLabels.SUBJECT_LABEL}
                            </label>
                            <Field
                                className="text-right mb-4"
                                name={authAttributes.SUBJECT_ATTRIBUTE}
                                options={subjects}
                                component={CustomSelect}
                                placeholder={authPlaceholders.SUBJECT_PLACEHOLDER}
                                isMulti={true}
                            />
                            <ErrorMessage
                                name={authAttributes.SUBJECT_ATTRIBUTE}
                                component={TextError as ComponentType} />
                        </div>

                        <div className="flex flex-col">
                            <label htmlFor={authAttributes.PASSWORD_ATTRIBUTE} className="text-right mb-4 font-bold">
                                <span className="text-red-500">*</span>
                                {authLabels.PASSWORD_LABEL}
                            </label>
                            <Field
                                type="password"
                                className="text-right border-2 p-2 text-black mb-4"
                                name={authAttributes.PASSWORD_ATTRIBUTE}
                                placeholder={authPlaceholders.PASSWORD_PLACEHOLDER}
                            />
                            <ErrorMessage
                                name={authAttributes.PASSWORD_ATTRIBUTE}
                                component={TextError as ComponentType} />
                        </div>


                        <div className="flex flex-col">
                            <label htmlFor={authAttributes.CONFIRM_PASSWORD_ATTRIBUTE} className="text-right mb-4 font-bold">
                                <span className="text-red-500">*</span>
                                {authLabels.CONFIRM_PASSWORD_LABEL}
                                </label>
                            <Field
                                type="password"
                                className="text-right border-2 p-2 text-black  mb-4"
                                name={authAttributes.CONFIRM_PASSWORD_ATTRIBUTE}
                                placeholder={authPlaceholders.PASSWORD_PLACEHOLDER}
                            />
                            <ErrorMessage
                                name={authAttributes.CONFIRM_PASSWORD_ATTRIBUTE}
                                component={TextError as ComponentType} />

                        </div>

                        <button
                            type="submit"
                            className={`mt-8 px-4 py-2 cursor-pointer text-white rounded-lg text-2xl font-bold shadow-lg  flex items-center justify-center ${!loading ? "bg-darkGreen hover:bg-mainGreen" : "bg-gray-400"}`}
                            disabled={loading}
                        >
                            <ClipLoader
                                color="white"
                                loading={loading}
                                size={36}
                                aria-label="Loading Spinner"
                                data-testid="loader"
                            />
                            <span className="ml-4">اشتراك</span>
                        </button>
                    </Form>
                )}
            </Formik>

        </div>
    );
};
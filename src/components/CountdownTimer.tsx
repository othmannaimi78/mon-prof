import React, { useEffect, useState } from 'react';

// interfaces
import { CalendarData } from '@/interfaces/calendarData';

interface CountdownTimerProps {
  data: CalendarData;
}

const CountdownTimer: React.FC<CountdownTimerProps> = ({ data }) => {
  
  const startTime = new Date(`${data.month}/${data.dayNumber}/${data.year} ${data.startTime}`);
  // States
  const [timeLeft, setTimeLeft] = useState<number>(0);

  useEffect(() => {
    const intervalId = setInterval(() => {
      const currentTime = new Date();
      const timeDiff = startTime.getTime() - currentTime.getTime();
      setTimeLeft(() => {
        const updatedTimeLeft = Math.ceil(timeDiff / 1000);
        if (updatedTimeLeft <= 0) {
          clearInterval(intervalId);
        }
        return updatedTimeLeft;
      });
    }, 1000);
  
    return () => {
      clearInterval(intervalId);
    };
  }, [data.startTime]);  

  const formatTimeLeft = (time: number): string => {
    const days = Math.floor(time / (3600 * 24));
    const hours = Math.floor((time % (3600 * 24)) / 3600);
    const minutes = Math.floor((time % 3600) / 60);
    const seconds = time % 60;

    return `${days.toString().padStart(2, '0')}:${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
  };

  const renderLink = () => {
    if (timeLeft > 0) {
      return <span className='cursor-not-allowed'>
        {timeLeft > 0 && (
          <>
            {formatTimeLeft(timeLeft)}
          </>
        )}
      </span>;
    } else {
      return <a href={data.inviteLink} target='blank' className='text-white bg-mainGreen cursor-pointer px-4 py-2 text-center rounded-sm'>اذهب الى الفصل</a>;
    }
  };

  return (
    <div className='flex flex-col md:flex-row justify-between items-center my-4 md:flex-wrap mb-2'>
    <div className='mb-5 md:mb-0 md:mr-4'>
      {data.dayNumber}/{data.month}/{data.year} {data.startTime} - {data.endTime}    
    </div>
    <div className='mb-5 md:mb-0'>
      {renderLink()}
    </div>
    <div className='uppercase'>
      {data.firstName} {data.lastName}
    </div>
  </div>
  
  );
};

export default CountdownTimer;

import React, { useState, useEffect } from 'react';
import axios from 'axios';

// Constants APIs
import { ZOOM_MEETING_API } from '@/constants/api';

export const Countdown: React.FC<{ launchTime: number }> = ({ launchTime }) => {
	// States
	const [countdown, setCountdown] = useState<string>('');
	const [jonLink, setJoinLink] = useState<string>('#');

	const padStart = (time: number): string => time.toString().padStart(2, '0');
	const createZoomMeeting = async () => {
		// join_url
		// TODO: add try + catch + logs
		// TODO: func should return join_link url
		const response = await axios.post(ZOOM_MEETING_API);
		setJoinLink(response.data.result.join_url);
		console.log("countdown done, link should be generated");
		console.log("jonLink: ", jonLink);
	};

	const CLASS_STARTED = "بدأ الفصل";

	useEffect(() => {
		const countdownInterval = setInterval(async() => {
			const currentTime = new Date().getTime();
			const timeRemaining = launchTime - currentTime;

			if (timeRemaining <= 0) {
				createZoomMeeting();
				clearInterval(countdownInterval);
                setCountdown(CLASS_STARTED);
				return;
			}

			// Calculate remaining time
			const seconds = Math.floor((timeRemaining / 1000) % 60);
			const minutes = Math.floor((timeRemaining / 1000 / 60) % 60);
			const hours = Math.floor((timeRemaining / (1000 * 60 * 60)) % 24);
			const days = Math.floor(timeRemaining / (1000 * 60 * 60 * 24));

			// Construct countdown string
			const countdownString = `تبدأ الحصة بعد ${days.toString().padStart(2, '0')}:${padStart(
				hours
			)}:${padStart(minutes)}:${padStart(seconds)}`;

			setCountdown(countdownString);
		}, 1000);

		return () => clearInterval(countdownInterval);
	}, [launchTime]);

    return (
        <a href={jonLink} target='_blank'>
			<span className={`text-white flex items-center px-4 py-2 rounded-sm cursor-pointer ${countdown != CLASS_STARTED ? "bg-gray-500" : "bg-darkGreen"}`}>{countdown}</span>
		</a>
	);
};

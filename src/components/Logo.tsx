import Image from "next/image";
import Link from "next/link";

export const Logo: React.FC<{ href: string; }> = ({ href }) =>  {
    return (
        <Link href={href}>
            <Image 
                width={180}
                height={180}
                alt="logo"
                src="/logo.png"
            />
        </Link>
    );
};
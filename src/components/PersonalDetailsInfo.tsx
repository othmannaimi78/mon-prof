import { ComponentType, useEffect, useState } from 'react';
import {
	collection,
	getDocs,
	query,
	where,
	updateDoc,
	doc,
} from 'firebase/firestore';
import { User } from 'firebase/auth';
import { useAuthState, useUpdateEmail } from 'react-firebase-hooks/auth';
import { ClipLoader } from 'react-spinners';
import { ErrorMessage, Field, Form, Formik } from 'formik';

// Firebase config
import { auth, db } from '@/firebaseConfig';

// Types
import { UserInfo } from '@/types';

// Constants
import { authLabels } from '@/constants/authLabels';
import { authAttributes } from '@/constants/authAttributes';
import { authPlaceholders } from '@/constants/authPlaceholders';

// Schema
import { UpdateProfileValues } from '@/schema/profile';

// Components
import TextError from './TextError';

// Schema validation
import { updateProfileSchemaValidation } from '@/schema/profileValidation';
import { showToast } from 'react-next-toast';

export const PersonalDetailsInfo: React.FC = () => {

	// firebase hooks
	const [user, loading] = useAuthState(auth);

	// handle updateEmail error
	const [updateEmail] = useUpdateEmail(auth);

	// states
	const [userInfo, setUserInfo] = useState<UserInfo | null>(null);

	const initialValues: UpdateProfileValues = {
		email: userInfo?.email || '',
		name: userInfo?.name || '',
		phoneNumber: userInfo?.phoneNumber || '',
	};

	const fetchUserInfo = async (user: User | null | undefined) => {
		try {
			const q = query(
                collection(db, 'users'), 
                where('uid', '==', user?.uid)
            );
			const doc = await getDocs(q);
			const data = doc.docs[0].data() as UserInfo;
			setUserInfo(data);
		} catch (error: unknown) {
			console.error(`fetch user info got error: ${error}`);
		};
	};

	const updateProfile = async (values: UpdateProfileValues) => {
		if (user) {
			try {
				await updateEmail(values.email);
				const q = query(collection(db, 'users'), where('uid', '==', user?.uid));
				const data = await getDocs(q);
				const docRef = doc(db, 'users', data.docs[0].id);
				await updateDoc(docRef, {
					name: values.name,
					email: values.email,
					phoneNumber: values.phoneNumber,
				});
				showToast.success('تم التحديث بنجاح');
			} catch (error: unknown) {
				console.log(`update profile got an error: ${error}`);
			};
		};
	};

	useEffect(() => {
		fetchUserInfo(user);
	}, [user, loading]);

	return (
		<div className='text-right h-full px-8 md:px-24'>
			<h1 className='mb-8 md:text-3xl md:mb-24 mt-8'>
				<strong>{userInfo?.name}</strong>, مرحباً
			</h1>
			<Formik
				initialValues={initialValues}
				validationSchema={updateProfileSchemaValidation}
				onSubmit={updateProfile}
				enableReinitialize>
				{() => (
					<Form className='flex flex-col shadow-formShadow rounded-lg px-8 pb-16 h-fit text-gray1 sm:w-11/12 md:w-1/2 md:mx-auto xl:w-full'>
						<div className='flex flex-col'>
							<label
								htmlFor={authAttributes.NAME_ATTRIBUTE}
								className='text-right mb-4 font-bold'>
								<span className="text-red-500">*</span>
								{authLabels.NAME_LABEL}
							</label>
							<Field
								type='text'
								className='text-right border-2 p-2 text-black mb-4'
								name={authAttributes.NAME_ATTRIBUTE}
								placeholder={authPlaceholders.NAME_PLACEHOLDER}
							/>
							<ErrorMessage
								name={authAttributes.NAME_ATTRIBUTE}
								component={TextError as ComponentType}
							/>
						</div>

						<div className='flex flex-col'>
							<label
								htmlFor={authAttributes.EMAIL_ATTRIBUTE}
								className='text-right mb-4 font-bold'>
								<span className="text-red-500">*</span>
								{authLabels.EMAIL_LABEL}
							</label>
							<Field
								type='text'
								className='text-right border-2 p-2 text-black mb-4'
								name={authAttributes.EMAIL_ATTRIBUTE}
								placeholder={authPlaceholders.EMAIL_PLACEHOLDER}
							/>
							<ErrorMessage
								name={authAttributes.EMAIL_ATTRIBUTE}
								component={TextError as ComponentType}
							/>
						</div>

						<div className='flex flex-col'>
							<label
								htmlFor={authAttributes.PHONE_NUMBER_ATTRIBUTE}
								className='text-right mb-4 font-bold'>
								<span className="text-red-500">*</span>
								{authLabels.PHONE_NUMBER_LABEL}
							</label>
							<Field
								type='text'
								className='text-right border-2 p-2 text-black mb-4'
								name={authAttributes.PHONE_NUMBER_ATTRIBUTE}
								placeholder={authPlaceholders.PHONE_NUMBER_PLACEHOLDER}
							/>
							<ErrorMessage
								name={authAttributes.PHONE_NUMBER_ATTRIBUTE}
								component={TextError as ComponentType}
							/>
						</div>
						<button
							type='submit'
							className={`mt-8 px-4 py-2 cursor-pointer text-white rounded-lg text-2xl font-bold shadow-lg  flex items-center justify-center ${
								!loading ? 'bg-darkGreen hover:bg-mainGreen' : 'bg-gray-400'
							}`}
							disabled={loading}>
							<ClipLoader
								color='white'
								loading={loading}
								size={36}
								aria-label='Loading Spinner'
								data-testid='loader'
							/>
							<span className='ml-4'>احفظ التغييرات</span>
						</button>
					</Form>
				)}
			</Formik>
		</div>
	);
};

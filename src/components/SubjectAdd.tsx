import { ComponentType, useEffect, useState } from 'react';
import Modal from './Modal';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import { authAttributes } from '@/constants/authAttributes';
import * as Yup from 'yup';
import { messageErrors } from '@/constants/messageErrors';
import { authLabels } from '@/constants/authLabels';
import CustomSelect from './CustomSelect';
import { authPlaceholders } from '@/constants/authPlaceholders';
import TextError from './TextError';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth, db } from '@/firebaseConfig';
import {
	collection,
	doc,
	getDocs,
	query,
	updateDoc,
	where,
} from 'firebase/firestore';
import { Category, SubjectData, subjects } from '@/constants/categories';
import { ClipLoader } from 'react-spinners';
import { showToast } from 'react-next-toast';

export const SubjectAdd: React.FC<{
	currentSubjects: SubjectData[];
	setCurrentSubjects: (subjects: SubjectData[]) => void;
}> = ({ currentSubjects, setCurrentSubjects }) => {
	// firebase hooks
	const [user, loading] = useAuthState(auth);

	// States
	const [isOpen, setIsOpen] = useState<boolean>(false);
	const [isLoadingAddOrUpdateSubject, setIsLoadingAddOrUpdateSubject] =
		useState<boolean>(false);

	const initialValues = {
		subjects: currentSubjects,
	};

	// TODO: change name of this later
	const uploadFileSchemaValidation = Yup.object({
		subjects: Yup.array()
			.min(1, messageErrors.FIELD_REQUIRED_MESSAGE_ERROR)
			.required(messageErrors.FIELD_REQUIRED_MESSAGE_ERROR),
	});

	const openModal = () => {
		setIsOpen(true);
	};

	// TODO: should add type for values
	const addOrUpdateSubjects = async (values: any) => {
		setIsLoadingAddOrUpdateSubject(true);
		try {
			const q = query(collection(db, 'users'), where('uid', '==', user?.uid));
			const data = await getDocs(q);
			const docRef = doc(db, 'users', data.docs[0].id);
			setCurrentSubjects(values.subjects);
			await updateDoc(docRef, {
				subjects: values.subjects,
			}).then(() => {
				setIsLoadingAddOrUpdateSubject(false);
				close();
				showToast.success('تم تحديث المواد ');
			});
		} catch (error: unknown) {
			setIsLoadingAddOrUpdateSubject(false);
			console.log(`update subjects got an error: ${error}`);
		}
	};

	// TODO: move this logic to the store

	const close = () => setIsOpen(false);

	return (
		<>
			<div className='w-full flex items-center justify-center h-32'>
				<button
					onClick={openModal}
					className='w-24 h-24 flex items-center justify-center text-5xl cursor-pointer bg-white shadow-rectangleShadow rounded-full'>
					+
				</button>
			</div>
			<Modal isOpen={isOpen}>
				<Formik
					initialValues={initialValues}
					validationSchema={uploadFileSchemaValidation}
					onSubmit={addOrUpdateSubjects}>
					{(props) => (
						<Form>
							<h1 className='text-3xl'>إضافة أو تحديث</h1>
							<div className='flex flex-col'>
								<label
									htmlFor={authAttributes.SUBJECT_ATTRIBUTE}
									className='text-right mb-4 font-bold'>
									<span className='text-red-500'>*</span>
									{authLabels.SUBJECT_LABEL}
								</label>
								<ErrorMessage
									name={authAttributes.SUBJECT_ATTRIBUTE}
									component={TextError as ComponentType}
								/>
								<Field
									className='text-right mb-4'
									name={authAttributes.SUBJECT_ATTRIBUTE}
									options={subjects.filter(
										(subject: Category) =>
											!currentSubjects.some(
												(currentSubject: SubjectData) =>
													currentSubject.value === subject.value
											)
									)}
									component={CustomSelect}
									placeholder={authPlaceholders.SUBJECT_PLACEHOLDER}
									isMulti={true}
									defaultValues={currentSubjects}
								/>
							</div>
							<button
								className={`w-full my-5 px-4 py-2 cursor-pointer text-white rounded-lg text-2xl font-bold shadow-lg  flex items-center justify-center ${
									!isLoadingAddOrUpdateSubject
										? 'bg-darkGreen hover:bg-mainGreen'
										: 'bg-gray-400'
								}`}
								type='submit'>
								<ClipLoader
									color='white'
									loading={isLoadingAddOrUpdateSubject}
									size={36}
									aria-label='Loading Spinner'
									data-testid='loader'
								/>
								احفظ التغييرات
							</button>
							<button
								className='w-full mt-8 px-4 py-2 cursor-pointer rounded-lg text-2xl font-bold shadow-lg  flex items-center justify-center bg-gray-500 text-white'
								type='button'
								onClick={close}>
								إلغاء
							</button>
						</Form>
					)}
				</Formik>
			</Modal>
		</>
	);
};

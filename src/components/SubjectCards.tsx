import { useEffect, useState } from "react";
import { User } from "firebase/auth";
import { collection, getDocs, query, where } from "firebase/firestore";
import { useAuthState } from "react-firebase-hooks/auth";

// Firebase config
import { auth, db } from "@/firebaseConfig";

// Components
import { SubjectAdd } from "./SubjectAdd";
import { SubjectCard } from "./SubjectCard";

// Types
import { UserInfo } from "@/types";
import { ClipLoader } from "react-spinners";
import { SubjectData } from "@/constants/categories";

export const SubjectCards: React.FC = () => {

    // firebase hooks
    const [user, loading, error] = useAuthState(auth);
    
    // States
    const [subjects, setSubjects] = useState<SubjectData[]>([]);
    
    const fetchUserSubjects = async (user: User | null | undefined) => {
    try {
      const q = query(collection(db, "users"), where("uid", "==", user?.uid));
      const doc = await getDocs(q);
      const data = doc.docs[0].data() as UserInfo;
      setSubjects(data.subjects);
    } catch (err) {
      console.error(err);
    }
    };

    useEffect(() => {
      fetchUserSubjects(user);
  }, [user, loading]);
    
    return (
        <>
            {!loading ? (
                <>
                    <div className="grid grid-cols-1 md:grid-cols-3">
                        {
                            subjects.map((subject: SubjectData, index: number) => (
                                <SubjectCard 
                                    key={index} 
                                    subjectTitle={subject.label} 
                                    subjectLink={subject.slug} 
                                />
                            ))
                        }
                    </div>
                    <SubjectAdd currentSubjects={subjects} setCurrentSubjects={setSubjects} /> 
                </>
            ) : <div className="flex items-center justify-center w-full">
                    <ClipLoader
                    color="black"
                    loading={loading}
                    size={36}
                    aria-label="Loading Spinner"
                    data-testid="loader"
            />
            </div>
            }
        </>
    );
};
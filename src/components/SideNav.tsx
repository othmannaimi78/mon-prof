import Link from 'next/link';
import Image from 'next/image';
import { usePathname, useRouter } from 'next/navigation';

// Constants
import { Route, dashboardRoutes } from '@/constants/routes';

// Components
import { Logo } from './Logo';
import { logout } from '@/firebaseConfig';

export const SideNav: React.FC = () => {
	
    // Hooks
	const router = useRouter();
    const pathname = usePathname();

    // TODO: needs to be cleaned
	const onLogout = () => {
		logout();
		router.push('/login');
	};

	return (
		<nav className='w-1/5 h-full bg-white fixed top-0 right-0 p-4'>
			<div className='flex items-center justify-center'>
				<Logo href='/dashboard' />
			</div>
			<div className='mt-14'>
				{dashboardRoutes.map((route: Route) => (
					<Link
						key={route.id}
						href={route.href}>
						<div 
                        
                        className={`py-4 flex items-center justify-center cursor-pointer hover:text-darkGreen md:justify-end ${pathname === route.href ? "text-darkGreen" : "text-black"}`}
                        >
							<span className='mr-4 text-xl hidden md:block'>
								{route.title}
							</span>
							<div className={`${pathname === route.href ? "bg-black" : "bg-white"}`}>
							<Image
								width={24}
								height={24}
								alt='icon'
								src={route.icon as string}
							/>
							</div>
						</div>
					</Link>
				))}
			</div>
			<div
				className='py-4 flex items-center justify-center cursor-pointer hover:text-darkGreen md:justify-end'
				onClick={onLogout}>
				<span className='mr-4 text-xl hidden md:block'>تسجيل الخروج</span>
				<Image
					width={24}
					height={24}
					alt='icon'
					src='/icons/signout-icon.svg'
				/>

			</div>
		</nav>
	);
};

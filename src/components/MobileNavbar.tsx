import Image from 'next/image';
import Link from 'next/link';

// Components
import { Logo } from './Logo';
import { usePathname } from 'next/navigation';
import { Drawer } from './Drawer';
import { useState } from 'react';
import { Route, mainRoutes } from '@/constants/routes';

export const MobileNavbar: React.FC = () => {
	// Hooks
	const pathname = usePathname();

	// States
	const [isOpen, setIsOpen] = useState<boolean>(false);

	const onClose = () => setIsOpen(false);

	const onOpen = () => setIsOpen(true);

	return (
		<nav className='h-24 flex justify-between items-center md:hidden'>
			<div className='flex items-center space-x-2'>
				<Image
					width={24}
					height={24}
					alt='menu-icon'
					src='/icons/menu-alt-04-svgrepo-com.svg'
					onClick={onOpen}
				/>
				<Drawer
					isOpen={isOpen}
					onClose={onClose}>
					<div className='flex flex-col items-center'>
						{mainRoutes.reverse().map((routerLink: Route) => (
							<Link
								key={routerLink.id}
								href={routerLink.href}>
								<p
									className={`mb-5 text-black text-2xl font-bold ${
										pathname === routerLink.href
											? 'text-darkGreen'
											: 'text-black '
									}`}>
									{routerLink.title}
								</p>
							</Link>
						))}
					</div>
				</Drawer>
				{pathname === '/login' ? (
					<Link href='/register'>
						<button className='cursor-pointer rounded-full bg-darkGreen text-white px-4 py-2 text-base flex items-center justify-center'>
							سجل مجانا
						</button>
					</Link>
				) : (
					<Link href='/login'>
						<span className='cursor-pointer rounded-full bg-darkGreen text-white px-4 py-2 text-base flex items-center justify-center'>
							دخول
						</span>
					</Link>
				)}
			</div>
			<Logo href='/' />
		</nav>
	);
};

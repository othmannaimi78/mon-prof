import {
	ChangeEvent,
	FormEvent,
	useEffect,
	useState,
} from 'react';

// Layouts
import { isValidSize, isValidType } from '@/utils/validateFile';
import { getDownloadURL, ref, uploadBytesResumable } from 'firebase/storage';
import { auth, db, storage } from '@/firebaseConfig';
import {
	collection,
	doc,
	getDocs,
	query,
	updateDoc,
	where,
} from 'firebase/firestore';
import { useAuthState } from 'react-firebase-hooks/auth';
import { showToast } from 'react-next-toast';

// Components
import Modal from './Modal';
import { messageErrors } from '@/constants/messageErrors';
import { User } from 'firebase/auth';
import { UserInfo } from '@/types';
import { SubjectData } from '@/constants/categories';
import { ClipLoader } from 'react-spinners';

// TODO: should move logic to smart component
export const SubcribeForm: React.FC = () => {
	// States
	const [file, setFile] = useState<File | undefined>(undefined);
	const [messageError, setMessageError] = useState<string | null>(null);
	const [progresspercent, setProgresspercent] = useState<number>(0);
	const [isOpen, setIsOpen] = useState<boolean>(false);
	const [subjects, setSubjects] = useState<SubjectData[]>([]);
	const [isPaymentUploaded, setIsPaymentUploaded] = useState<boolean>(false);

	// firebase hooks
	const [user, loading] = useAuthState(auth);

	const handleFile = (event: ChangeEvent<HTMLInputElement>): void => {
		
		const file = event.target.files?.[0];

		if (isValidType(file?.type)) {
			setMessageError(null);
			if (isValidSize(file?.size)) {
				setMessageError(null);
				setFile(file);
			} else {
				setMessageError('يسمح الملف 5 ميغا بايت فقط');
			}
		} else {
			setMessageError('صيغة الملف غير مدعومة');
		}
	};

	const submitFileToStorage = (file?: File): string | undefined  => {
		if (!file) return;

		const storageRef = ref(storage, `files/${file.name}`);
		const uploadTask = uploadBytesResumable(storageRef, file);

		uploadTask.on(
			'state_changed',
			(snapshot) => {
				const progress = Math.round(
					(snapshot.bytesTransferred / snapshot.totalBytes) * 100
				);
				setProgresspercent(progress);
			},
			(error) => {
				alert(error);
			},
			() => {
				getDownloadURL(uploadTask.snapshot.ref).then(async(downloadURL: string) => {
					try {
						const q = query(
							collection(db, 'users'),
							where('uid', '==', user?.uid)
						);
						const data = await getDocs(q);
						const docRef = doc(db, 'users', data.docs[0].id);
						await updateDoc(docRef, {
							docUrl: downloadURL,
						});
						close();
						setIsPaymentUploaded(false);
						showToast.success('تم تحميل وثيقة الدفع');
					} catch (error: unknown) {
						console.log(`update doc url got an error: ${error}`);
					}
				});
			}
		);
	};


	const submitFile = async (event: FormEvent) => {

		event.preventDefault();
		
		if (!file) {
			setMessageError(messageErrors.FIELD_REQUIRED_MESSAGE_ERROR);
		} else {
			if (user) {
				setIsPaymentUploaded(true);
				setMessageError(null);
				submitFileToStorage(file);
			}
		}
	};

	const displayTotal = (data: SubjectData[]): number => {
		const totalPrice = data.reduce((acc, obj: SubjectData) => {
			if (obj.hasOwnProperty('price') && obj.price) {
				return acc + obj.price;
			} else {
				return acc;
			}
		}, 0);

		return totalPrice;
	};

	const openModal = () => {
		setIsOpen(true);
	};

	const close = () => setIsOpen(false);

	const fetchUserSubjects = async (user: User | null | undefined) => {
		try {
			const q = query(collection(db, 'users'), where('uid', '==', user?.uid));
			const doc = await getDocs(q);
			const data = doc.docs[0].data() as UserInfo;
			setSubjects(data.subjects);
		} catch (err) {
			setIsPaymentUploaded(false);
			console.error(err);
		}
	};

	const displaySubjects = (subjects: SubjectData[]) => {
		return (
			<div className='flex flex-wrap items-center my-4'>
				{
					subjects.map((subject: SubjectData) => (
						<span className='text-center p-4 border-2 mx-4' key={subject.uuid}>{subject.label}</span>
					))
				}
			</div>
		)
	}

	useEffect(() => {
		fetchUserSubjects(user);
	}, [user, loading]);

	return (
		<>
			<button
				onClick={openModal}
				className='w-full mt-8 px-4 py-2 cursor-pointer text-white rounded-lg text-2xl font-bold shadow-lg  flex items-center justify-center bg-darkGreen'>
				إشترك الأن
			</button>
			<Modal isOpen={isOpen}>
				<form>
					<div className='flex flex-col'>
						<label className='text-right mb-4 font-bold'>
							<span className='text-red-500'>*</span>
							التوصيل
						</label>
						<p className='text-red-500'>{messageError && messageError}</p>
						<input
							type='file'
							name='fileUpload'
							accept='.jpg, .jpeg, .png, .pdf'
							onChange={handleFile}
						/>
					</div>
					<div>{displaySubjects(subjects)}</div>
					<div>Total: {displayTotal(subjects)}DH</div>
					<button
						className={`w-full mt-8 px-4 py-2 cursor-pointer text-white rounded-lg text-2xl font-bold shadow-lg  flex items-center justify-center bg-darkGreen ${
							!isPaymentUploaded
								? 'bg-darkGreen hover:bg-mainGreen'
								: 'bg-gray-400'
						}`}
						type='submit'
						onClick={submitFile}>
							<ClipLoader
									color='white'
									loading={isPaymentUploaded}
									size={36}
									aria-label='Loading Spinner'
									data-testid='loader'
								/>
						حمل
					</button>
					<button
						className='w-full mt-8 px-4 py-2 cursor-pointer rounded-lg text-2xl font-bold shadow-lg  flex items-center justify-center bg-gray-500 text-white'
						type='button'
						onClick={close}>
						إلغاء
					</button>
				</form>
			</Modal>
		</>
	);
};

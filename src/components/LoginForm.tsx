'use client'
import { ErrorMessage, Field, Form, Formik, FormikProps } from "formik";
import { ClipLoader } from "react-spinners";
import { ComponentType, useState } from "react";
import { useRouter } from "next/navigation";

// firebase config
import { logInWithEmailAndPassword } from "../firebaseConfig";

// Schema
import { loginValues } from "@/schema/auth";
import { logInSchemaValidation } from "@/schema/authValidation";

// Components
import TextError from "./TextError";

// Constants
import { authAttributes } from "@/constants/authAttributes";
import { authPlaceholders } from "@/constants/authPlaceholders";
import { authLabels } from "@/constants/authLabels";

export const LoginForm: React.FC = () => {

    // States
    const [loading, setLoading] = useState<boolean>(false);
    const [firebaseErrorMessage, setFirebaseErrorMessage] = useState<string | null>(null);

    // Hooks
    const router = useRouter();
    
    const initialValues: loginValues = {
        email: "",
        password: ""
    };

    const login = async (values: loginValues) => {
        setLoading(true);
        try {
            await logInWithEmailAndPassword(values)
            console.log("firebase login with success");
            setLoading(false);
            setFirebaseErrorMessage(null);
            router.push("/dashboard");
        } catch (error: unknown) {
            if (error instanceof Error) {
                if (error.message === "auth/user-not-found") {
                    setFirebaseErrorMessage("البريد الإلكتروني أو كلمة المرور غير صحيحة");
                };
            } else {
                setFirebaseErrorMessage("من فضلك حاول لاحقا");
            };
            setLoading(false);
        };
    };

    return (
        <div className="xl:w-1/2 mt-10 mx-auto">
            <h1 className="text-center font-bold text-5xl mb-8">!إبدأ أسبوع مجانا</h1>
            <Formik
                initialValues={initialValues}
                validationSchema={logInSchemaValidation}
                onSubmit={login}
            >
                {(props: FormikProps<any>) => (
                    <Form className="flex flex-col shadow-formShadow rounded-lg px-8 pb-16 h-fit text-gray1 sm:w-11/12 md:w-1/2 md:mx-auto xl:w-full">
                        <p className="text-right text-red-500 my-8">{firebaseErrorMessage && firebaseErrorMessage}</p>
                        <div className="flex flex-col">
                            <label htmlFor={authAttributes.EMAIL_ATTRIBUTE} className="text-right mb-4 font-bold">
                                <span className="text-red-500">*</span>
                                {authLabels.EMAIL_LABEL}
                            </label>
                            <Field
                                type="text"
                                className="text-right border-2 p-2 text-black mb-4"
                                name={authAttributes.EMAIL_ATTRIBUTE}
                                placeholder={authPlaceholders.EMAIL_PLACEHOLDER}
                            />
                            <ErrorMessage
                                name={authAttributes.EMAIL_ATTRIBUTE}
                                component={TextError as ComponentType}
                            />
                        </div>


                        <div className="flex flex-col">
                            <label htmlFor={authAttributes.PASSWORD_ATTRIBUTE} className="text-right mb-4 font-bold">
                                <span className="text-red-500">*</span>
                                {authLabels.PASSWORD_LABEL}
                            </label>
                            <Field
                                type="password"
                                className="text-right border-2 p-2 text-black mb-4"
                                name={authAttributes.PASSWORD_ATTRIBUTE}
                                placeholder={authPlaceholders.PASSWORD_PLACEHOLDER}
                            />
                            <ErrorMessage
                                name={authAttributes.PASSWORD_ATTRIBUTE}
                                component={TextError as ComponentType} />
                        </div>


                        <button
                            type="submit"
                            className={`mt-8 px-4 py-2 cursor-pointer text-white rounded-lg text-2xl font-bold shadow-lg  flex items-center justify-center ${!loading ? "bg-darkGreen hover:bg-mainGreen" : "bg-gray-400"}`}
                            disabled={loading}
                        >
                            <ClipLoader
                                color="white"
                                loading={loading}
                                size={36}
                                aria-label="Loading Spinner"
                                data-testid="loader"
                            />
                            <span className="ml-4">دخول</span>
                        </button>
                    </Form>
                )}
            </Formik>

        </div>
    );
};
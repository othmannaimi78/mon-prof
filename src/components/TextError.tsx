const TextError: React.FC<{ children: JSX.Element }> = ({ children }) => (
    <p className='text-red-500 text-right'>{children}</p>
);

export default TextError;
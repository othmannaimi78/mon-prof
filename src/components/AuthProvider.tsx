import { auth } from "@/firebaseConfig";
import { redirect } from "next/navigation";
import { useEffect } from "react";
import { useAuthState } from "react-firebase-hooks/auth";

export const AuthProvider: React.FC<{ children: JSX.Element }> = ({ children }) => {

    const [user] = useAuthState(auth);

    useEffect(() => {
        console.log("user null: ", user);

        if (user) {
          console.log("signed in!");
        } else if (user == null) {
          return redirect("/login");
        }
      }, [user]);
    
      if (!user) {
        console.log("user null: ", user);
        // user is signed out or still being checked.
        // don't render anything
        return null;
      }

    return (
        <>
            {children}
        </>
    );
};
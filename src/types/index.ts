// Constants
import { SubjectData } from '@/constants/categories';

export type UserInfo = {
	name: string;
	authProvider: string;
	email: string;
	level: string;
	subjects: SubjectData[];
	phoneNumber: string;
	uid: string;
};

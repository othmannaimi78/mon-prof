import axios from 'axios';
import qs from 'query-string';

// Constants
import { ZOOM_OAUTH_ENDPOINT } from '../constants/api';

export const getToken = async () => {
	try {
		const { ZOOM_ACCOUNT_ID, ZOOM_CLIENT_ID, ZOOM_CLIENT_SECRET } = process.env;

		const request = await axios.post(
			ZOOM_OAUTH_ENDPOINT,
			qs.stringify({
				grant_type: 'account_credentials',
				account_id: ZOOM_ACCOUNT_ID,
			}),
			{
				headers: {
					Authorization: `Basic ${Buffer.from(
						`${ZOOM_CLIENT_ID}:${ZOOM_CLIENT_SECRET}`
					).toString('base64')}`,
				},
			}
		);

		const { access_token, expires_in } = await request.data;

		return { access_token, expires_in, error: null };
	} catch (error) {
		return { access_token: null, expires_in: null, error };
	}
};

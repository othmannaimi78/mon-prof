export interface CalendarData {
	firstName: string;
	lastName: string;
	startTime: string;
	endTime: string;
	dayNumber?: string;
	inviteLink?: string;
	month?: string;
	year?: string;
	subjectID: string;
	uuid: string;
	dayName: string;
}

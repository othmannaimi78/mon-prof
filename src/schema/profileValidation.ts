import * as Yup from "yup";

// Constants
import { messageErrors } from "@/constants/messageErrors";

export const updateProfileSchemaValidation = Yup.object({
  name: Yup.string()
    .min(3)
    .max(50)
    .required(messageErrors.FIELD_REQUIRED_MESSAGE_ERROR),
  email: Yup.string()
    .email(messageErrors.EMAIL_NOT_VALID_MESSAGE_ERROR)
    .required(messageErrors.FIELD_REQUIRED_MESSAGE_ERROR),
  phoneNumber: Yup.string().required(
    messageErrors.FIELD_REQUIRED_MESSAGE_ERROR
  ),
});

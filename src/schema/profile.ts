export interface UpdateProfileValues {
  name: string;
  email: string;
  phoneNumber: string;
}

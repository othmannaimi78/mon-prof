import * as Yup from 'yup';

// Constants
import { messageErrors } from '@/constants/messageErrors';

export const registerSchemaValidation = Yup.object({
	name: Yup.string()
		.min(3)
		.max(50)
		.required(messageErrors.FIELD_REQUIRED_MESSAGE_ERROR),
	email: Yup.string()
		.email(messageErrors.EMAIL_NOT_VALID_MESSAGE_ERROR)
		.required(messageErrors.FIELD_REQUIRED_MESSAGE_ERROR),
	level: Yup.string().required(messageErrors.FIELD_REQUIRED_MESSAGE_ERROR),
	subjects: Yup.array().required(messageErrors.FIELD_REQUIRED_MESSAGE_ERROR),
	phoneNumber: Yup.string().required(
		messageErrors.FIELD_REQUIRED_MESSAGE_ERROR
	),
	password: Yup.string()
		.min(8, messageErrors.PASSWORD_MAX_LENGTH_MESSAGE_ERROR)
		.required(messageErrors.FIELD_REQUIRED_MESSAGE_ERROR),
	confirmPassword: Yup.string()
		.oneOf(
			[Yup.ref('password')],
			messageErrors.PASSWORD_NOT_MATCH_MESSAGE_ERROR
		)
		.required(messageErrors.FIELD_REQUIRED_MESSAGE_ERROR),
});

export const logInSchemaValidation = Yup.object({
	email: Yup.string()
		.email(messageErrors.EMAIL_NOT_VALID_MESSAGE_ERROR)
		.required(messageErrors.FIELD_REQUIRED_MESSAGE_ERROR),
	password: Yup.string()
		.min(8, messageErrors.PASSWORD_MAX_LENGTH_MESSAGE_ERROR)
		.required(messageErrors.FIELD_REQUIRED_MESSAGE_ERROR),
});

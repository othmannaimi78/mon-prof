interface Subject {
	uuid: string;
	label: string;
	value: string;
}

export interface RegisterValues {
	name: string;
	email: string;
	phoneNumber: string;
	level: string;
	subjects: Subject[];
	password: string;
	confirmPassword: string;
	creationDate: string;
	isSubscribed: boolean;
	subscriptionDate: string | null;
	docUrl: string | null;
}

export interface loginValues {
	email: string;
	password: string;
}

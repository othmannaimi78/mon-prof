import { NextPage } from "next";

const NotFound: NextPage = () => {
    return (
        <main>
            <h1>الصفحة غير موجودة</h1>
        </main>
    );
};

export default NotFound;
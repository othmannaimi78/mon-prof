'use client';
import { NextPage } from "next";

// Components
import { LoginForm } from "@/components/LoginForm";

// Layouts
import { MainLayout } from "@/layouts/MainLyout";
import Head from "next/head";

const Login: NextPage = () => {
    return (
        <MainLayout>
            <>
            <Head>
				<title>التسجيل و الدخول</title>
			</Head>
            <LoginForm />
            </>
        </MainLayout>
    );
};

export default Login;
'use client';
import { NextPage } from "next";

// Components
import { RegisterForm } from "@/components/RegisterForm";

// Layouts
import { MainLayout } from "@/layouts/MainLyout";


const Register: NextPage = () => {
    return (
        <MainLayout>
            <RegisterForm />
        </MainLayout>
    );
};

export default Register;
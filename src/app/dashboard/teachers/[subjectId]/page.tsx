'use client';
import { NextPage } from 'next';
import { redirect, useParams } from 'next/navigation';

// Dashboard Layout
import { useEffect } from 'react';

// Dashboard layout
import { DashboardLayout } from '@/layouts/DashboardLayout';

// Components
import { TeachersList } from '@/components/TeachersList';
import { Calendar } from '@/components/Calendar';

// TODO: should convert it as export component as well
import CountdownTimer from '@/components/CountdownTimer';

// Constants
import { Subject, subjects } from '@/constants/subjects';
import { timeSlots } from '@/constants/calendarData';
import { nextClasses } from '@/constants/nextClasses';
import { CalendarData } from '@/interfaces/calendarData';

const Teachers: NextPage = () => {
	// params hooks
	const { subjectId } = useParams();

	const subject = subjects.find(
		(subject: Subject) => subject.slug === subjectId
	);
	const filteredCurrentTeacherBySubjectID = nextClasses.filter(
		(currentTeacher: CalendarData) => currentTeacher.subjectID === subjectId
	);

	useEffect(() => {
		if (!subject) redirect('/404');
	}, []);

	return (
		<DashboardLayout>
			<div className='h-full w-[80%] pb-24 overflow-y-auto'>
				<div className='h-full md:px-24'>
					<h1 className='text-center text-3xl mt-8 font-bold mb-5 pr-5 md:mb-24'>
						{subject?.name}
					</h1>
					<TeachersList teachersList={subject?.teachers} />
					<h1 className='text-center text-3xl mt-14 font-bold mb-5 pr-5 md:mb-24'>
						توقيت زمني
					</h1>
					<Calendar
						data={filteredCurrentTeacherBySubjectID}
						timeSlots={timeSlots}
					/>
					<h1 className='text-center text-3xl mt-14 font-bold mb-5 pr-5 md:mb-24'>
						الحصص القادمة
					</h1>
					<div className='py-8'>
						{nextClasses
							.filter(
								(nextClasse: CalendarData) => nextClasse.subjectID === subjectId
							)
							.map((nextClasse: CalendarData) => (
								<CountdownTimer
									key={nextClasse.uuid}
									data={nextClasse}
								/>
							))}
					</div>
				</div>
			</div>
		</DashboardLayout>
	);
};

export default Teachers;

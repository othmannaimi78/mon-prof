'use client';
import { NextPage } from "next";

// Layouts
import { DashboardLayout } from "@/layouts/DashboardLayout";

// Components
import { SubjectCards } from "@/components/SubjectCards";
import { SubcribeForm } from "@/components/SubcribeForm";

const Subjects: NextPage = () => {
    return (
        <DashboardLayout>
            <div className='h-full w-[80%]'>
                <div className="h-full text-right md:px-24">
                    <h1 className="text-3xl mt-8 font-bold mb-5 pr-5 md:mb-24">المواد</h1>
                    <SubjectCards />
                    <SubcribeForm />
                </div>
            </div>
        </DashboardLayout>
    );
};

export default Subjects;
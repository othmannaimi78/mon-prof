'use client'
import { NextPage } from 'next';

// Dashboard Layout
import { DashboardLayout } from '@/layouts/DashboardLayout';

// Components
import { PersonalDetailsInfo } from '@/components/PersonalDetailsInfo';

const Dashboard: NextPage = () => {
  
  return (
    <DashboardLayout>
      <div className='h-full w-[85%] pb-24 lg-w-[80%]'>
          <PersonalDetailsInfo />
      </div>
    </DashboardLayout>
  );
};

export default Dashboard;

'use client';
import { NextPage } from 'next';

// Components
import { Hero } from '@/components/Hero';

// Layouts
import { MainLayout } from '@/layouts/MainLyout';
import Head from 'next/head';

const Home: NextPage = () => {
	return (
    <MainLayout>
        <>
			<Head>
				<title>ناجح</title>
			</Head>
				<Hero />
		</>
			</MainLayout>
	);
};

export default Home;

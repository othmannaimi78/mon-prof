import { NextResponse } from 'next/server';
import axios from 'axios';

// Services
import { getToken } from '@/services/token';

// Constants
import { ZOOM_API_BASE_URL } from '@/constants/api';

export async function POST() {
	const { access_token } = await getToken();
	const headerConfig = {
		headers: {
			Authorization: `Bearer ${access_token}`,
		},
	};
	const body = {
		recurrence: {
			end_date_time: '2023-05-20T20:00:00Z',
			end_times: 7,
			monthly_day: 1,
			monthly_week: 1,
			monthly_week_day: 1,
			repeat_interval: 1,
			type: 1,
			weekly_days: '1',
		},
		host_email: 'elbeqqal.youssef@gmail.com',
		settings: {
			approval_type: 2,
			breakout_room: {
				enable: true,
				rooms: [
					{
						name: 'room1',
						participants: ['elbeqqal.youssef@gmail.com'],
					},
				],
			},
			calendar_type: 2,
			email_notification: true,
			encryption_type: 'enhanced_encryption',
			focus_mode: true,
			host_video: true,
			jbh_time: 0,
			join_before_host: false,
			meeting_authentication: true,
			meeting_invitees: [
				{
					email: 'elbeqqal.youssef@gmail.com',
				},
			],
			private_meeting: true,
		},
		start_time: '2023-05-22T17:32:55Z',
		timezone: 'WEST',
		topic: 'MATH',
		type: 2,
		allow_multiple_devices: true,
	};
	try {
		const request = await axios.post(
			`${ZOOM_API_BASE_URL}/users/me/meetings`,
			body,
			headerConfig
		);
		return NextResponse.json({ result: request.data });
	} catch (err) {
		return NextResponse.json({
			message_error: 'Error creating meeting for user',
		});
	}
}

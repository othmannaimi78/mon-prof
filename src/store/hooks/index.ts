// Libraries
import { createTypedHooks } from 'easy-peasy';

// Store Model
import { StoreModel } from '../models';

const typedHooks = createTypedHooks<StoreModel>();

export const useStoreActions = typedHooks.useStoreActions;
export const useStoreDispatch = typedHooks.useStoreDispatch;
export const useStoreState = typedHooks.useStoreState;
import { createStore } from "easy-peasy";
import { StoreModel } from "./models";

// Stores
const store = createStore<StoreModel>({});

export default store;
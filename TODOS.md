## TASKS:

- [ ] Custom input upload file doc
- [ ] add title for each page
- [ ] display email status
- [ ] In case there is no teachers display message there
- [ ] In case there is not classes display message there
- [ ] check upload payment file.

- [ ] add login link in register page
- [ ] add register link in login page
- [ ] add ability to show password
- [ ] if user update email signout + redirect to login page
- [ ] make teachers page responsive
- [ ] custom loading in dasboard page
- [ ] add policy check to the register form
- [ ] add about content
- [ ] Forgot password.

- [ ] Display poppup when payement will be expire soon or 1 ween expire soon.
- [ ] check user creation_date when he/she try to access resources
  - [ ] If he/she have account more than 1 week or is_subscribed not true or subscription_date passed 1 month. print warning console and tell him should be pay. otherwise let him access.
  - [ ] create dummy form for pay.
  - [ ] show form for pay that contain upload file.
- [ ] close student account after 1 week if and ask him/here to pay for continue studying (form will discuss it with othman later)
- [ ] custome countdown design
- [ ] add already account link in register form
- [ ] display list of links + login in mobile menu
- [ ] replace console log with watson logs
- [ ] add error code to common file later for firebase server errors
- [ ] add SEO keywords
- [ ] display email verification status in dashboard page
- [ ] clean console logs
- [ ] handle this error: CREDENTIAL_TOO_OLD_LOGIN_AGAIN
- [ ] show message when email changed
- [ ] add list of text in hero section as component
- [ ] add loading state when data profile changed
- [ ] adjust design for personal details page
- [ ] adjust design for subjects page
- [ ] adjust design for teachers page
- [ ] Move padStart to util folder
- [ ] make teachers list section responsive
- [ ] make table of calendar responsive in mobile
- [ ] make invite link + countdown responsive in mobile
- [ ] add form for upload payment invoice for validate account
- [ ] Replace console log with other secure loggs system.
- [ ] check delay while you navigate through application.
- [ ] add register link into login form.
- [ ] check deactivated account from firebase.

# Notes:

- user can change email and benifits from free week again. should be careful in this step.

# Improvements

- [ ] Check errors and warning in console.log.
- [ ] check left todos
- [ ] Fix code format.
- [ ] verify frontend performance
- [ ] generate invite link for all teachers related to that subject
- [ ] should verify if link generated ok and cnnection with server are ok if not should re-try again.
- [ ] In Hero component cleanup inline style and try to use tailwindCss classes
- [ ] remove unused packages
- [ ] Improve code using GPT
- [ ] add eslint file for format code.
- [ ] convert images to webp
- [ ] add unit test for each component
- [ ] add e2e testing
- [ ] add ability to see password by click on eyes icon
- [ ] add footer
- [ ] Customize 404 page
  - [ ] Should think in design
  - [ ] handle 404 page for auth users and guest users
- [ ] add contact page
- [ ] add policy page
- [ ] change title for each page
- [ ] NextJs best practices
- [ ] add ability to submit by enter
- [ ] add green color in outline for inputs
- [ ] add middleware as protector for the app
- [ ] validate email after user register
- [ ] add title to n8i for add translation later
- [ ] reset password
- [ ] fix warning issues in build
- [ ] check this error later

## Testing workflow

- [ ] Create new account.
- [ ] Can't access to other content without auth.
- [ ] Can't access to content without payement after one week.
- [ ] Can see teachers related to specific subject.
- [ ] Can upload payement prof.
- [ ] Can see next classes.
- [ ] Can see countdown.
- [ ] Test login and logout.
- [ ] Check responsive for landing page.
- [ ] Check responsive for login + signup.
- [ ] Check contact us responsive.
- [ ] Check subjects responsive.
- [ ] Check teachers responsive.
- [ ] check warning in build.
- [ ] create hello world pipeline.
- [ ] change name of prject in my local machine and gitlab as well.

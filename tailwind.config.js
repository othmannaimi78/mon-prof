/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
    './src/layouts/**/*.{js,ts,jsx,tsx,mdx}'
  ],
  theme: {
    extend: {
      container: {
        center: true,
        padding: {
          DEFAULT: "1rem",
          sm: "2rem",
          lg: "4rem",
          xl: "5rem",
          "2xl": "6rem",
        },
      },
      colors: {
        mainGreen: "#147361",
        darkGreen: "#0d6252",
        mainYellow: "#F2C230",
        mainOrange: "#F29829",
        mainGray: "#F0F0F2",
        mainBlue: "#0A0E26"
      },
      boxShadow: {
        formShadow: "12px 32px 60px -8px rgba(0,0,0,0.12)",
        rectangleShadow: "rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px"
      },
    },
  },
  plugins: [],
}
